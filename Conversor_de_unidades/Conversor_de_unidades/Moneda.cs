﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversor_de_unidades
{
    public partial class Moneda : Form
    {
        bool P_in;
        bool E_in;
        bool D_in;
        bool S_in;
        bool Y_in;
        bool B_in;
        double val_ent;
        public Moneda()
        {
            InitializeComponent();
            
        }
        private void Peso_in_Click(object sender, EventArgs e)
        {
            P_in = true;

        }
        private void Euro_in_Click(object sender, EventArgs e)
        {
            E_in = true;
        }

        private void Dollar_in_Click(object sender, EventArgs e)
        {
            D_in = true;
        }

        private void Sol_in_Click(object sender, EventArgs e)
        {
            S_in = true;
        }

        private void Yen_in_Click(object sender, EventArgs e)
        {
            Y_in = true;
        }

        private void Bolivar_in_Click(object sender, EventArgs e)
        {
            B_in = true;
        }
        private void Euro_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (P_in == true)
            {
                double P_E = val_ent / 21.45;
                tb_out.Text = P_E.ToString(); 
                P_in = false;
            }
            if (E_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                E_in = false;
            }
            if (D_in == true)
            {
                double D_E = val_ent / 1.10;
                tb_out.Text = D_E.ToString();
                D_in = false;
            }
            if (S_in == true)
            {
                double S_E = val_ent / 3.73;
                tb_out.Text = S_E.ToString();
                S_in = false;
            }
            if (Y_in == true)
            {
                double Y_E = val_ent / 120.13;
                tb_out.Text = Y_E.ToString();
                Y_in = false;
            }
            if (B_in == true)
            {
                double B_E = val_ent / 35892.36;
                tb_out.Text = B_E.ToString();
                B_in = false;
            }
        }
       

        private void Peso_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (P_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                P_in = false;
            }
            if (E_in == true)
            {
                double E_P = val_ent / 0.047;
                tb_out.Text = E_P.ToString();
                D_in = false;
            }
            if (D_in == true)
            {
                double D_P = val_ent / 0.051;
                tb_out.Text = D_P.ToString();
                D_in = false;
            }
            if (S_in == true)
            {
                double S_P = val_ent / 0.17;
                tb_out.Text = S_P.ToString();
                S_in = false;
            }
            if (Y_in == true)
            {
                double Y_P = val_ent / 5.60;
                tb_out.Text = Y_P.ToString();
                Y_in = false;
            }
            if (B_in == true)
            {
                double B_P = val_ent / 1672.40;
                tb_out.Text = B_P.ToString();
                B_in = false;
            }
        }

        private void Dollar_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (P_in == true)
            {
                double P_D = val_ent / 19.50;
                tb_out.Text = P_D.ToString();
                P_in = false;
            }
            if (E_in == true)
            {
                double E_D = val_ent / 0.91;
                tb_out.Text = E_D.ToString();
                P_in = false;
            }
            if (D_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                D_in = false;
            }
            if (S_in == true)
            {
                double S_D = val_ent / 3.38;
                tb_out.Text = S_D.ToString();
                S_in = false;
            }
            if (Y_in == true)
            {
                double Y_D = val_ent / 109.14;
                tb_out.Text = Y_D.ToString();
                Y_in = false;
            }
            if (B_in == true)
            {
                double B_D = val_ent / 32602.00;
                tb_out.Text = B_D.ToString();
                B_in = false;
            }
        }

        private void Sol_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (P_in == true)
            {
                double P_S = val_ent / 5.76;
                tb_out.Text = P_S.ToString();
                P_in = false;
            }
            if (E_in == true)
            {
                double E_S = val_ent / 0.27;
                tb_out.Text = E_S.ToString();
                P_in = false;
            }
            if (D_in == true)
            {
                double D_S = val_ent / 0.30;
                tb_out.Text = D_S.ToString();
                D_in = false;
            }
            if (S_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                S_in = false;
            }
            if (Y_in == true)
            {
                double Y_S = val_ent / 32.26;
                tb_out.Text = Y_S.ToString();
                Y_in = false;
            }
            if (B_in == true)
            {
                double B_S = val_ent / 9637.67;
                tb_out.Text = B_S.ToString();
                B_in = false;
            }
        }

        private void Yen_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (P_in == true)
            {
                double P_Y = val_ent / 0.18;
                tb_out.Text = P_Y.ToString();
                P_in = false;
            }
            if (E_in == true)
            {
                double E_Y = val_ent / 0.0083;
                tb_out.Text = E_Y.ToString();
                P_in = false;
            }
            if (D_in == true)
            {
                double D_Y = val_ent / 0.0092;
                tb_out.Text = D_Y.ToString();
                D_in = false;
            }
            if (S_in == true)
            {
                double S_Y = val_ent / 0.031;
                tb_out.Text = S_Y.ToString();
                S_in = false;
            }
            if (Y_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                Y_in = false;
            }
            if (B_in == true)
            {
                double B_Y = val_ent / 298.72;
                tb_out.Text = B_Y.ToString();
                B_in = false;
            }
        }

        private void Bolivar_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (P_in == true)
            {
                double P_B = val_ent / 0.00058;
                tb_out.Text = P_B.ToString();
                P_in = false;
            }
            if (E_in == true)
            {
                double E_B = val_ent / 0.000027;
                tb_out.Text = E_B.ToString();
                P_in = false;
            }
            if (D_in == true)
            {
                double D_B = val_ent / 0.000030;
                tb_out.Text = D_B.ToString();
                D_in = false;
            }
            if (S_in == true)
            {
                double S_B = val_ent / 0.00010;
                tb_out.Text = S_B.ToString();
                S_in = false;
            }
            if (Y_in == true)
            {
                double Y_B = val_ent / 0.0032;
                tb_out.Text = Y_B.ToString();
                Y_in = false;
            }
            if (B_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                B_in = false;
            }
        }

        private void Moneda_Load(object sender, EventArgs e)
        {

        }
    }
}
