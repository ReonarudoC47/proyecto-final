﻿namespace Conversor_de_unidades
{
    partial class Volumen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_out = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_in = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Barril_in = new System.Windows.Forms.Button();
            this.Pinta_in = new System.Windows.Forms.Button();
            this.Mili_in = new System.Windows.Forms.Button();
            this.Gal_in = new System.Windows.Forms.Button();
            this.Onza_in = new System.Windows.Forms.Button();
            this.Litro_in = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Barril_out = new System.Windows.Forms.Button();
            this.Pinta_out = new System.Windows.Forms.Button();
            this.ML_out = new System.Windows.Forms.Button();
            this.Gal_out = new System.Windows.Forms.Button();
            this.Onza_out = new System.Windows.Forms.Button();
            this.Litro_out = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_out
            // 
            this.tb_out.Location = new System.Drawing.Point(202, 204);
            this.tb_out.Name = "tb_out";
            this.tb_out.ReadOnly = true;
            this.tb_out.Size = new System.Drawing.Size(146, 20);
            this.tb_out.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(199, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Valor a cambiado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Nuevo volumen";
            // 
            // tb_in
            // 
            this.tb_in.Location = new System.Drawing.Point(202, 78);
            this.tb_in.Name = "tb_in";
            this.tb_in.Size = new System.Drawing.Size(146, 20);
            this.tb_in.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(183, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Valor a cambiar (usar \",\" para decimales):";
            // 
            // Barril_in
            // 
            this.Barril_in.Location = new System.Drawing.Point(93, 104);
            this.Barril_in.Name = "Barril_in";
            this.Barril_in.Size = new System.Drawing.Size(75, 23);
            this.Barril_in.TabIndex = 30;
            this.Barril_in.Text = "Barril";
            this.Barril_in.UseVisualStyleBackColor = true;
            this.Barril_in.Click += new System.EventHandler(this.Barril_in_Click);
            // 
            // Pinta_in
            // 
            this.Pinta_in.Location = new System.Drawing.Point(93, 75);
            this.Pinta_in.Name = "Pinta_in";
            this.Pinta_in.Size = new System.Drawing.Size(75, 23);
            this.Pinta_in.TabIndex = 29;
            this.Pinta_in.Text = "Pinta";
            this.Pinta_in.UseVisualStyleBackColor = true;
            this.Pinta_in.Click += new System.EventHandler(this.Pinta_in_Click);
            // 
            // Mili_in
            // 
            this.Mili_in.Location = new System.Drawing.Point(93, 46);
            this.Mili_in.Name = "Mili_in";
            this.Mili_in.Size = new System.Drawing.Size(75, 23);
            this.Mili_in.TabIndex = 28;
            this.Mili_in.Text = "Mililitro";
            this.Mili_in.UseVisualStyleBackColor = true;
            this.Mili_in.Click += new System.EventHandler(this.ML_in_Click);
            // 
            // Gal_in
            // 
            this.Gal_in.Location = new System.Drawing.Point(12, 104);
            this.Gal_in.Name = "Gal_in";
            this.Gal_in.Size = new System.Drawing.Size(75, 23);
            this.Gal_in.TabIndex = 27;
            this.Gal_in.Text = "Galón";
            this.Gal_in.UseVisualStyleBackColor = true;
            this.Gal_in.Click += new System.EventHandler(this.Gal_in_Click);
            // 
            // Onza_in
            // 
            this.Onza_in.Location = new System.Drawing.Point(12, 75);
            this.Onza_in.Name = "Onza_in";
            this.Onza_in.Size = new System.Drawing.Size(75, 23);
            this.Onza_in.TabIndex = 26;
            this.Onza_in.Text = "Onza";
            this.Onza_in.UseVisualStyleBackColor = true;
            this.Onza_in.Click += new System.EventHandler(this.Onza_in_Click);
            // 
            // Litro_in
            // 
            this.Litro_in.Location = new System.Drawing.Point(12, 46);
            this.Litro_in.Name = "Litro_in";
            this.Litro_in.Size = new System.Drawing.Size(75, 23);
            this.Litro_in.TabIndex = 25;
            this.Litro_in.Text = "Litro";
            this.Litro_in.UseVisualStyleBackColor = true;
            this.Litro_in.Click += new System.EventHandler(this.Litro_in_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Volumen a cambiar";
            // 
            // Barril_out
            // 
            this.Barril_out.Location = new System.Drawing.Point(93, 231);
            this.Barril_out.Name = "Barril_out";
            this.Barril_out.Size = new System.Drawing.Size(75, 23);
            this.Barril_out.TabIndex = 41;
            this.Barril_out.Text = "Barril";
            this.Barril_out.UseVisualStyleBackColor = true;
            // 
            // Pinta_out
            // 
            this.Pinta_out.Location = new System.Drawing.Point(93, 202);
            this.Pinta_out.Name = "Pinta_out";
            this.Pinta_out.Size = new System.Drawing.Size(75, 23);
            this.Pinta_out.TabIndex = 40;
            this.Pinta_out.Text = "Pinta";
            this.Pinta_out.UseVisualStyleBackColor = true;
            // 
            // ML_out
            // 
            this.ML_out.Location = new System.Drawing.Point(93, 173);
            this.ML_out.Name = "ML_out";
            this.ML_out.Size = new System.Drawing.Size(75, 23);
            this.ML_out.TabIndex = 39;
            this.ML_out.Text = "Mililitro";
            this.ML_out.UseVisualStyleBackColor = true;
            // 
            // Gal_out
            // 
            this.Gal_out.Location = new System.Drawing.Point(12, 231);
            this.Gal_out.Name = "Gal_out";
            this.Gal_out.Size = new System.Drawing.Size(75, 23);
            this.Gal_out.TabIndex = 38;
            this.Gal_out.Text = "Galón";
            this.Gal_out.UseVisualStyleBackColor = true;
            // 
            // Onza_out
            // 
            this.Onza_out.Location = new System.Drawing.Point(12, 202);
            this.Onza_out.Name = "Onza_out";
            this.Onza_out.Size = new System.Drawing.Size(75, 23);
            this.Onza_out.TabIndex = 37;
            this.Onza_out.Text = "Onza";
            this.Onza_out.UseVisualStyleBackColor = true;
            // 
            // Litro_out
            // 
            this.Litro_out.Location = new System.Drawing.Point(12, 173);
            this.Litro_out.Name = "Litro_out";
            this.Litro_out.Size = new System.Drawing.Size(75, 23);
            this.Litro_out.TabIndex = 36;
            this.Litro_out.Text = "Litro";
            this.Litro_out.UseVisualStyleBackColor = true;
            this.Litro_out.Click += new System.EventHandler(this.Litro_out_Click);
            // 
            // Volumen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 304);
            this.Controls.Add(this.Barril_out);
            this.Controls.Add(this.Pinta_out);
            this.Controls.Add(this.ML_out);
            this.Controls.Add(this.Gal_out);
            this.Controls.Add(this.Onza_out);
            this.Controls.Add(this.Litro_out);
            this.Controls.Add(this.tb_out);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_in);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Barril_in);
            this.Controls.Add(this.Pinta_in);
            this.Controls.Add(this.Mili_in);
            this.Controls.Add(this.Gal_in);
            this.Controls.Add(this.Onza_in);
            this.Controls.Add(this.Litro_in);
            this.Controls.Add(this.label1);
            this.Name = "Volumen";
            this.Text = "Volumen";
            this.Load += new System.EventHandler(this.Volumen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_out;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_in;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Barril_in;
        private System.Windows.Forms.Button Pinta_in;
        private System.Windows.Forms.Button Mili_in;
        private System.Windows.Forms.Button Gal_in;
        private System.Windows.Forms.Button Onza_in;
        private System.Windows.Forms.Button Litro_in;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Barril_out;
        private System.Windows.Forms.Button Pinta_out;
        private System.Windows.Forms.Button ML_out;
        private System.Windows.Forms.Button Gal_out;
        private System.Windows.Forms.Button Onza_out;
        private System.Windows.Forms.Button Litro_out;
    }
}