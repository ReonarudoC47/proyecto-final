﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversor_de_unidades
{
    public partial class Longitúd : Form
    {
        public Longitúd()
        {
            InitializeComponent();
        }
        bool K_in;
        bool Me_in;
        bool Mi_in;
        bool Pi_in;
        bool Pa_in;
        bool A_in;
        double val_ent;
        private void Kilo_in_Click(object sender, EventArgs e)
        {
            K_in = true;
        }

        private void Metro_in_Click(object sender, EventArgs e)
        {
            Me_in = true;
        }

        private void Milla_in_Click(object sender, EventArgs e)
        {
            Mi_in = true;
        }

        private void Pie_in_Click(object sender, EventArgs e)
        {
            Pi_in = true;
        }

        private void Par_in_Click(object sender, EventArgs e)
        {
            Pa_in = true;
        }

        private void AL_in_Click(object sender, EventArgs e)
        {
            A_in = true;
        }


        private void Metro_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (K_in == true)
            {
                double K_Me = val_ent / .001;
                tb_out.Text = K_Me.ToString();
                K_in = false;
            }
            if (Me_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                Me_in = false;
            }
            if (Mi_in == true)
            {
                double Mi_Me = val_ent / 0.000621;
                tb_out.Text = Mi_Me.ToString();
                Mi_in = false;
            }
            if (Pi_in == true)
            {
                double Pi_Me = val_ent / 3.280839;
                tb_out.Text = Pi_Me.ToString();
                Pi_in = false;
            }
            if (Pa_in == true)
            {
                double Pa_Me = val_ent / 0.00000000000000003240779;
                tb_out.Text = Pa_Me.ToString();
                Pa_in = false;
            }
            if (A_in == true)
            {
                double A_Me = val_ent / 0.0000000000000001057;
                tb_out.Text = A_Me.ToString();
                A_in = false;
            }
        }


        private void Kilo_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (K_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                K_in = false;
            }
            if (Me_in == true)
            {
                double Me_K = val_ent / 1000;
                tb_out.Text = Me_K.ToString();
                Me_in = false;
            }
            if (Mi_in == true)
            {
                double Mi_K = val_ent / 0.621371;
                tb_out.Text = Mi_K.ToString();
                Mi_in = false;
            }
            if (Pi_in == true)
            {
                double Pi_K = val_ent / 3280.839;
                tb_out.Text = Pi_K.ToString();
                Pi_in = false;
            }
            if (Pa_in == true)
            {
                double Pa_K = val_ent / .00000000000003240779;
                tb_out.Text = Pa_K.ToString();
                Pa_in = false;
            }
            if (A_in == true)
            {
                double A_K = val_ent / .0000000000001057;
                tb_out.Text = A_K.ToString();
                A_in = false;
            }
        }

        private void Milla_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (K_in == true)
            {
                double K_Mi = val_ent / 1.609344;
                tb_out.Text = K_Mi.ToString();
                K_in = false;
            }
            if (Me_in == true)
            {
                double Me_Mi = val_ent / 1609.344;
                tb_out.Text = Me_Mi.ToString();
                Me_in = false;
            }
            if (Mi_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                Mi_in = false;
            }
            if (Pi_in == true)
            {
                double Pi_Mi = val_ent / 5280;
                tb_out.Text = Pi_Mi.ToString();
                Pi_in = false;
            }
            if (Pa_in == true)
            {
                double Pa_Mi = val_ent / .00000000000005215528;
                tb_out.Text = Pa_Mi.ToString();
                Pa_in = false;
            }
            if (A_in == true)
            {
                double A_Mi = val_ent / .0000000000001701077;
                tb_out.Text = A_Mi.ToString();
                A_in = false;
            }
        }

        private void Pie_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (K_in == true)
            {
                double K_Pi = val_ent / 0.0003048;
                tb_out.Text = K_Pi.ToString();
                K_in = false;
            }
            if (Me_in == true)
            {
                double Me_Pi = val_ent / 0.3048;
                tb_out.Text = Me_Pi.ToString();
                Me_in = false;
            }
            if (Mi_in == true)
            {
                double Mi_Pi = val_ent / 0.0001893;
                tb_out.Text = Mi_Pi.ToString();
                Mi_in = false;
            }
            if (Pi_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                Pi_in = false;
            }
            if (Pa_in == true)
            {
                double Pa_Pi = val_ent / .000000000000000009877895;
                tb_out.Text = Pa_Pi.ToString();
                Pa_in = false;
            }
            if (A_in == true)
            {
                double A_Pi = val_ent / .00000000000000003221738;
                tb_out.Text = A_Pi.ToString();
                A_in = false;
            }
        }

        private void Par_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (K_in == true)
            {
                double K_Pa = val_ent / 30856775814913.7;
                tb_out.Text = K_Pa.ToString();
                K_in = false;
            }
            if (Me_in == true)
            {
                double Me_Pa = val_ent / 30856775814913700;
                tb_out.Text = Me_Pa.ToString();
                Me_in = false;
            }
            if (Mi_in == true)
            {
                double Mi_Pa = val_ent / 19173511576713;
                tb_out.Text = Mi_Pa.ToString();
                Mi_in = false;
            }
            if (Pi_in == true)
            {
                double Pi_Pa = val_ent / 10123614112504500;
                tb_out.Text = Pi_Pa.ToString();
                Pi_in = false;
            }
            if (Pa_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                Pa_in = false;
            }
            if (A_in == true)
            {
                double A_Pa = val_ent / 3.26156377716743;
                tb_out.Text = A_Pa.ToString();
                A_in = false;
            }
        }

        private void AL_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (K_in == true)
            {
                double K_A = val_ent / 9460730472580.8;
                tb_out.Text = K_A.ToString();
                K_in = false;
            }
            if (Me_in == true)
            {
                double Me_A = val_ent / 9460730472580800;
                tb_out.Text = Me_A.ToString();
                Me_in = false;
            }
            if (Mi_in == true)
            {
                double Mi_A = val_ent / 5878625373183.61;
                tb_out.Text = Mi_A.ToString();
                Mi_in = false;
            }
            if (Pi_in == true)
            {
                double Pi_A = val_ent / 31039141970409400;
                tb_out.Text = Pi_A.ToString();
                Pi_in = false;
            }
            if (Pa_in == true)
            {
                double Pa_A = val_ent / .30660139378555;
                tb_out.Text = Pa_A.ToString();
                Pa_in = false;
            }
            if (A_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                A_in = false;
            }
        }

        private void Longitúd_Load(object sender, EventArgs e)
        {

        }

       
    }
}
