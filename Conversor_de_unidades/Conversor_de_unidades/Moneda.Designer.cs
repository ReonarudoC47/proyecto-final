﻿namespace Conversor_de_unidades
{
    partial class Moneda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Peso_in = new System.Windows.Forms.Button();
            this.Dollar_in = new System.Windows.Forms.Button();
            this.Yen_in = new System.Windows.Forms.Button();
            this.Euro_in = new System.Windows.Forms.Button();
            this.Sol_in = new System.Windows.Forms.Button();
            this.Bolivar_in = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_in = new System.Windows.Forms.TextBox();
            this.tb_out = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Bolivar_out = new System.Windows.Forms.Button();
            this.Sol_out = new System.Windows.Forms.Button();
            this.Euro_out = new System.Windows.Forms.Button();
            this.Yen_out = new System.Windows.Forms.Button();
            this.Dollar_out = new System.Windows.Forms.Button();
            this.Peso_out = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Moneda a cambiar";
            // 
            // Peso_in
            // 
            this.Peso_in.Location = new System.Drawing.Point(12, 41);
            this.Peso_in.Name = "Peso_in";
            this.Peso_in.Size = new System.Drawing.Size(75, 23);
            this.Peso_in.TabIndex = 1;
            this.Peso_in.Text = "Peso(MX)";
            this.Peso_in.UseVisualStyleBackColor = true;
            this.Peso_in.Click += new System.EventHandler(this.Peso_in_Click);
            // 
            // Dollar_in
            // 
            this.Dollar_in.Location = new System.Drawing.Point(12, 70);
            this.Dollar_in.Name = "Dollar_in";
            this.Dollar_in.Size = new System.Drawing.Size(75, 23);
            this.Dollar_in.TabIndex = 2;
            this.Dollar_in.Text = "Dollar";
            this.Dollar_in.UseVisualStyleBackColor = true;
            this.Dollar_in.Click += new System.EventHandler(this.Dollar_in_Click);
            // 
            // Yen_in
            // 
            this.Yen_in.Location = new System.Drawing.Point(12, 99);
            this.Yen_in.Name = "Yen_in";
            this.Yen_in.Size = new System.Drawing.Size(75, 23);
            this.Yen_in.TabIndex = 3;
            this.Yen_in.Text = "Yen";
            this.Yen_in.UseVisualStyleBackColor = true;
            this.Yen_in.Click += new System.EventHandler(this.Yen_in_Click);
            // 
            // Euro_in
            // 
            this.Euro_in.Location = new System.Drawing.Point(93, 41);
            this.Euro_in.Name = "Euro_in";
            this.Euro_in.Size = new System.Drawing.Size(75, 23);
            this.Euro_in.TabIndex = 4;
            this.Euro_in.Text = "Euro ";
            this.Euro_in.UseVisualStyleBackColor = true;
            this.Euro_in.Click += new System.EventHandler(this.Euro_in_Click);
            // 
            // Sol_in
            // 
            this.Sol_in.Location = new System.Drawing.Point(93, 70);
            this.Sol_in.Name = "Sol_in";
            this.Sol_in.Size = new System.Drawing.Size(75, 23);
            this.Sol_in.TabIndex = 5;
            this.Sol_in.Text = "Sol";
            this.Sol_in.UseVisualStyleBackColor = true;
            this.Sol_in.Click += new System.EventHandler(this.Sol_in_Click);
            // 
            // Bolivar_in
            // 
            this.Bolivar_in.Location = new System.Drawing.Point(93, 99);
            this.Bolivar_in.Name = "Bolivar_in";
            this.Bolivar_in.Size = new System.Drawing.Size(75, 23);
            this.Bolivar_in.TabIndex = 6;
            this.Bolivar_in.Text = "Bolivar";
            this.Bolivar_in.UseVisualStyleBackColor = true;
            this.Bolivar_in.Click += new System.EventHandler(this.Bolivar_in_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(183, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Valor a cambiar (usar \",\" para decimales):";
            // 
            // tb_in
            // 
            this.tb_in.Location = new System.Drawing.Point(202, 73);
            this.tb_in.Name = "tb_in";
            this.tb_in.Size = new System.Drawing.Size(146, 20);
            this.tb_in.TabIndex = 8;
            // 
            // tb_out
            // 
            this.tb_out.Location = new System.Drawing.Point(202, 199);
            this.tb_out.Name = "tb_out";
            this.tb_out.ReadOnly = true;
            this.tb_out.Size = new System.Drawing.Size(146, 20);
            this.tb_out.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(199, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Valor a cambiado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Tipo de moneda";
            // 
            // Bolivar_out
            // 
            this.Bolivar_out.Location = new System.Drawing.Point(93, 225);
            this.Bolivar_out.Name = "Bolivar_out";
            this.Bolivar_out.Size = new System.Drawing.Size(75, 23);
            this.Bolivar_out.TabIndex = 23;
            this.Bolivar_out.Text = "Bolivar";
            this.Bolivar_out.UseVisualStyleBackColor = true;
            this.Bolivar_out.Click += new System.EventHandler(this.Bolivar_out_Click);
            // 
            // Sol_out
            // 
            this.Sol_out.Location = new System.Drawing.Point(93, 196);
            this.Sol_out.Name = "Sol_out";
            this.Sol_out.Size = new System.Drawing.Size(75, 23);
            this.Sol_out.TabIndex = 22;
            this.Sol_out.Text = "Sol";
            this.Sol_out.UseVisualStyleBackColor = true;
            this.Sol_out.Click += new System.EventHandler(this.Sol_out_Click);
            // 
            // Euro_out
            // 
            this.Euro_out.Location = new System.Drawing.Point(93, 167);
            this.Euro_out.Name = "Euro_out";
            this.Euro_out.Size = new System.Drawing.Size(75, 23);
            this.Euro_out.TabIndex = 21;
            this.Euro_out.Text = "Euro ";
            this.Euro_out.UseVisualStyleBackColor = true;
            this.Euro_out.Click += new System.EventHandler(this.Euro_out_Click);
            // 
            // Yen_out
            // 
            this.Yen_out.Location = new System.Drawing.Point(12, 225);
            this.Yen_out.Name = "Yen_out";
            this.Yen_out.Size = new System.Drawing.Size(75, 23);
            this.Yen_out.TabIndex = 20;
            this.Yen_out.Text = "Yen";
            this.Yen_out.UseVisualStyleBackColor = true;
            this.Yen_out.Click += new System.EventHandler(this.Yen_out_Click);
            // 
            // Dollar_out
            // 
            this.Dollar_out.Location = new System.Drawing.Point(12, 196);
            this.Dollar_out.Name = "Dollar_out";
            this.Dollar_out.Size = new System.Drawing.Size(75, 23);
            this.Dollar_out.TabIndex = 19;
            this.Dollar_out.Text = "Dollar";
            this.Dollar_out.UseVisualStyleBackColor = true;
            this.Dollar_out.Click += new System.EventHandler(this.Dollar_out_Click);
            // 
            // Peso_out
            // 
            this.Peso_out.Location = new System.Drawing.Point(12, 167);
            this.Peso_out.Name = "Peso_out";
            this.Peso_out.Size = new System.Drawing.Size(75, 23);
            this.Peso_out.TabIndex = 18;
            this.Peso_out.Text = "Peso(MX)";
            this.Peso_out.UseVisualStyleBackColor = true;
            this.Peso_out.Click += new System.EventHandler(this.Peso_out_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 251);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(271, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Tipo de cambio capturado el 26 de  Noviembre de 2019";
            // 
            // Moneda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 299);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Bolivar_out);
            this.Controls.Add(this.Sol_out);
            this.Controls.Add(this.Euro_out);
            this.Controls.Add(this.Yen_out);
            this.Controls.Add(this.Dollar_out);
            this.Controls.Add(this.Peso_out);
            this.Controls.Add(this.tb_out);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_in);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Bolivar_in);
            this.Controls.Add(this.Sol_in);
            this.Controls.Add(this.Euro_in);
            this.Controls.Add(this.Yen_in);
            this.Controls.Add(this.Dollar_in);
            this.Controls.Add(this.Peso_in);
            this.Controls.Add(this.label1);
            this.Name = "Moneda";
            this.Text = "Moneda";
            this.Load += new System.EventHandler(this.Moneda_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Peso_in;
        private System.Windows.Forms.Button Dollar_in;
        private System.Windows.Forms.Button Yen_in;
        private System.Windows.Forms.Button Euro_in;
        private System.Windows.Forms.Button Sol_in;
        private System.Windows.Forms.Button Bolivar_in;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_in;
        private System.Windows.Forms.TextBox tb_out;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Bolivar_out;
        private System.Windows.Forms.Button Sol_out;
        private System.Windows.Forms.Button Euro_out;
        private System.Windows.Forms.Button Yen_out;
        private System.Windows.Forms.Button Dollar_out;
        private System.Windows.Forms.Button Peso_out;
        private System.Windows.Forms.Label label5;
    }
}