﻿namespace Conversor_de_unidades
{
    partial class Masa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnct = new System.Windows.Forms.Button();
            this.btnquilate = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lblconversion = new System.Windows.Forms.Label();
            this.btnG = new System.Windows.Forms.Button();
            this.btnKg = new System.Windows.Forms.Button();
            this.btnOz = new System.Windows.Forms.Button();
            this.btnLb = new System.Windows.Forms.Button();
            this.btnT = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btngramo = new System.Windows.Forms.Button();
            this.btnkilo = new System.Windows.Forms.Button();
            this.btnonza = new System.Windows.Forms.Button();
            this.btnlibra = new System.Windows.Forms.Button();
            this.btnTon = new System.Windows.Forms.Button();
            this.lblunidad = new System.Windows.Forms.Label();
            this.tbValor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnct
            // 
            this.btnct.Location = new System.Drawing.Point(155, 206);
            this.btnct.Margin = new System.Windows.Forms.Padding(2);
            this.btnct.Name = "btnct";
            this.btnct.Size = new System.Drawing.Size(72, 21);
            this.btnct.TabIndex = 41;
            this.btnct.Text = "Quilate";
            this.btnct.UseVisualStyleBackColor = true;
            // 
            // btnquilate
            // 
            this.btnquilate.Location = new System.Drawing.Point(155, 70);
            this.btnquilate.Margin = new System.Windows.Forms.Padding(2);
            this.btnquilate.Name = "btnquilate";
            this.btnquilate.Size = new System.Drawing.Size(72, 20);
            this.btnquilate.TabIndex = 40;
            this.btnquilate.Text = "Quilate";
            this.btnquilate.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(271, 28);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "La conversion es de";
            // 
            // lblconversion
            // 
            this.lblconversion.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblconversion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblconversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblconversion.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblconversion.Location = new System.Drawing.Point(270, 52);
            this.lblconversion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblconversion.Name = "lblconversion";
            this.lblconversion.Size = new System.Drawing.Size(173, 19);
            this.lblconversion.TabIndex = 38;
            // 
            // btnG
            // 
            this.btnG.Location = new System.Drawing.Point(82, 206);
            this.btnG.Margin = new System.Windows.Forms.Padding(2);
            this.btnG.Name = "btnG";
            this.btnG.Size = new System.Drawing.Size(70, 21);
            this.btnG.TabIndex = 37;
            this.btnG.Text = "Gramo";
            this.btnG.UseVisualStyleBackColor = true;
            // 
            // btnKg
            // 
            this.btnKg.Location = new System.Drawing.Point(155, 180);
            this.btnKg.Margin = new System.Windows.Forms.Padding(2);
            this.btnKg.Name = "btnKg";
            this.btnKg.Size = new System.Drawing.Size(72, 21);
            this.btnKg.TabIndex = 36;
            this.btnKg.Text = "Kilogramo";
            this.btnKg.UseVisualStyleBackColor = true;
            // 
            // btnOz
            // 
            this.btnOz.Location = new System.Drawing.Point(82, 181);
            this.btnOz.Margin = new System.Windows.Forms.Padding(2);
            this.btnOz.Name = "btnOz";
            this.btnOz.Size = new System.Drawing.Size(69, 21);
            this.btnOz.TabIndex = 35;
            this.btnOz.Text = "Onza";
            this.btnOz.UseVisualStyleBackColor = true;
            // 
            // btnLb
            // 
            this.btnLb.Location = new System.Drawing.Point(13, 206);
            this.btnLb.Margin = new System.Windows.Forms.Padding(2);
            this.btnLb.Name = "btnLb";
            this.btnLb.Size = new System.Drawing.Size(64, 21);
            this.btnLb.TabIndex = 34;
            this.btnLb.Text = "Libra";
            this.btnLb.UseVisualStyleBackColor = true;
            // 
            // btnT
            // 
            this.btnT.Location = new System.Drawing.Point(13, 181);
            this.btnT.Margin = new System.Windows.Forms.Padding(2);
            this.btnT.Name = "btnT";
            this.btnT.Size = new System.Drawing.Size(64, 20);
            this.btnT.TabIndex = 33;
            this.btnT.Text = "Tonelada";
            this.btnT.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 158);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Seleccione unidad de conversion";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Seleccione la unidad a convertir";
            // 
            // btngramo
            // 
            this.btngramo.Location = new System.Drawing.Point(82, 70);
            this.btngramo.Margin = new System.Windows.Forms.Padding(2);
            this.btngramo.Name = "btngramo";
            this.btngramo.Size = new System.Drawing.Size(69, 20);
            this.btngramo.TabIndex = 30;
            this.btngramo.Text = "Gramo";
            this.btngramo.UseVisualStyleBackColor = true;
            // 
            // btnkilo
            // 
            this.btnkilo.Location = new System.Drawing.Point(155, 41);
            this.btnkilo.Margin = new System.Windows.Forms.Padding(2);
            this.btnkilo.Name = "btnkilo";
            this.btnkilo.Size = new System.Drawing.Size(72, 20);
            this.btnkilo.TabIndex = 29;
            this.btnkilo.Text = "Kilogramo";
            this.btnkilo.UseVisualStyleBackColor = true;
            // 
            // btnonza
            // 
            this.btnonza.Location = new System.Drawing.Point(82, 41);
            this.btnonza.Margin = new System.Windows.Forms.Padding(2);
            this.btnonza.Name = "btnonza";
            this.btnonza.Size = new System.Drawing.Size(69, 20);
            this.btnonza.TabIndex = 28;
            this.btnonza.Text = "Onza";
            this.btnonza.UseVisualStyleBackColor = true;
            // 
            // btnlibra
            // 
            this.btnlibra.Location = new System.Drawing.Point(13, 70);
            this.btnlibra.Margin = new System.Windows.Forms.Padding(2);
            this.btnlibra.Name = "btnlibra";
            this.btnlibra.Size = new System.Drawing.Size(64, 20);
            this.btnlibra.TabIndex = 27;
            this.btnlibra.Text = "Libra";
            this.btnlibra.UseVisualStyleBackColor = true;
            // 
            // btnTon
            // 
            this.btnTon.Location = new System.Drawing.Point(13, 41);
            this.btnTon.Margin = new System.Windows.Forms.Padding(2);
            this.btnTon.Name = "btnTon";
            this.btnTon.Size = new System.Drawing.Size(64, 20);
            this.btnTon.TabIndex = 26;
            this.btnTon.Text = "Tonelada";
            this.btnTon.UseVisualStyleBackColor = true;
            // 
            // lblunidad
            // 
            this.lblunidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblunidad.Location = new System.Drawing.Point(91, 132);
            this.lblunidad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblunidad.Name = "lblunidad";
            this.lblunidad.Size = new System.Drawing.Size(60, 18);
            this.lblunidad.TabIndex = 25;
            // 
            // tbValor
            // 
            this.tbValor.Location = new System.Drawing.Point(13, 129);
            this.tbValor.Margin = new System.Windows.Forms.Padding(2);
            this.tbValor.Name = "tbValor";
            this.tbValor.Size = new System.Drawing.Size(77, 20);
            this.tbValor.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 104);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Ingrese el valor a convertir";
            // 
            // Masa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 239);
            this.Controls.Add(this.btnct);
            this.Controls.Add(this.btnquilate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblconversion);
            this.Controls.Add(this.btnG);
            this.Controls.Add(this.btnKg);
            this.Controls.Add(this.btnOz);
            this.Controls.Add(this.btnLb);
            this.Controls.Add(this.btnT);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btngramo);
            this.Controls.Add(this.btnkilo);
            this.Controls.Add(this.btnonza);
            this.Controls.Add(this.btnlibra);
            this.Controls.Add(this.btnTon);
            this.Controls.Add(this.lblunidad);
            this.Controls.Add(this.tbValor);
            this.Controls.Add(this.label1);
            this.Name = "Masa";
            this.Text = "Masa";
            this.Load += new System.EventHandler(this.Masa_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnct;
        private System.Windows.Forms.Button btnquilate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblconversion;
        private System.Windows.Forms.Button btnG;
        private System.Windows.Forms.Button btnKg;
        private System.Windows.Forms.Button btnOz;
        private System.Windows.Forms.Button btnLb;
        private System.Windows.Forms.Button btnT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btngramo;
        private System.Windows.Forms.Button btnkilo;
        private System.Windows.Forms.Button btnonza;
        private System.Windows.Forms.Button btnlibra;
        private System.Windows.Forms.Button btnTon;
        private System.Windows.Forms.Label lblunidad;
        private System.Windows.Forms.TextBox tbValor;
        private System.Windows.Forms.Label label1;
    }
}