﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversor_de_unidades
{
    public partial class Volumen : Form
    {
        public Volumen()
        {
            InitializeComponent();
        }
        bool L_in;
        bool M_in;
        bool O_in;
        bool P_in;
        bool G_in;
        bool B_in;
        double val_ent;
        private void Litro_in_Click(object sender, EventArgs e)
        {
            L_in = true;
        }

        private void ML_in_Click(object sender, EventArgs e)
        {
            M_in = true;
        }

        private void Onza_in_Click(object sender, EventArgs e)
        {
            O_in = true;
        }

        private void Pinta_in_Click(object sender, EventArgs e)
        {
            P_in = true;
        }

        private void Gal_in_Click(object sender, EventArgs e)
        {
            G_in = true;
        }

        private void Barril_in_Click(object sender, EventArgs e)
        {
            B_in = true;
        }


        private void ML_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (L_in == true)
            {
                double L_M = val_ent /.001;
                tb_out.Text = L_M.ToString();
                L_in = false;
            }
            if (M_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                M_in = false;
            }
            if (O_in == true)
            {
                double O_M = val_ent / 0.033814;
                tb_out.Text = O_M.ToString();
                O_in = false;
            }
            if (P_in == true)
            {
                double P_M = val_ent /.00211337;
                tb_out.Text = P_M.ToString();
                P_in = false;
            }
            if (G_in == true)
            {
                double G_M = val_ent /.000264172;
                tb_out.Text = G_M.ToString();
                G_in = false;
            }
            if (B_in == true)
            {
                double B_M = val_ent / 0.00000628981;
                tb_out.Text = B_M.ToString();
                B_in = false;
            }
        }


        private void Litro_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (L_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                L_in = false;
            }
            if (M_in == true)
            {
                double M_L = val_ent /1000;
                tb_out.Text = M_L.ToString();
                M_in = false;
            }
            if (O_in == true)
            {
                double O_L = val_ent /33.814;
                tb_out.Text = O_L.ToString();
                O_in = false;
            }
            if (P_in == true)
            {
                double P_L = val_ent / 2.1133;
                tb_out.Text = P_L.ToString();
                P_in = false;
            }
            if (G_in == true)
            {
                double G_L = val_ent / 2.6417;
                tb_out.Text = G_L.ToString();
                G_in = false;
            }
            if (B_in == true)
            {
                double B_L = val_ent / 0.00628981;
                tb_out.Text = B_L.ToString();
                B_in = false;
            }
        }

        private void Onza_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (L_in == true)
            {
                double L_O = val_ent / 0.02957;
                tb_out.Text = L_O.ToString();
                L_in = false;
            }
            if (M_in == true)
            {
                double M_O = val_ent / 29.57353;
                tb_out.Text = M_O.ToString();
                M_in = false;
            }
            if (O_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                O_in = false;
            }
            if (P_in == true)
            {
                double P_O = val_ent / 0.0625;
                tb_out.Text = P_O.ToString();
                P_in = false;
            }
            if (G_in == true)
            {
                double G_O = val_ent / 0.0078125;
                tb_out.Text = G_O.ToString();
                G_in = false;
            }
            if (B_in == true)
            {
                double B_O = val_ent / 0.00018601;
                tb_out.Text = B_O.ToString();
                B_in = false;
            }
        }

        private void Pinta_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (L_in == true)
            {
                double L_P = val_ent / 0.473176;
                tb_out.Text = L_P.ToString();
                L_in = false;
            }
            if (M_in == true)
            {
                double M_P = val_ent / 473.1765;
                tb_out.Text = M_P.ToString();
                M_in = false;
            }
            if (O_in == true)
            {
                double O_P = val_ent / 16;
                tb_out.Text = O_P.ToString();
                O_in = false;
            }
            if (P_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                P_in = false;
            }
            if (G_in == true)
            {
                double G_P = val_ent / 0.125;
                tb_out.Text = G_P.ToString();
                G_in = false;
            }
            if (B_in == true)
            {
                double B_P = val_ent / 0.002976;
                tb_out.Text = B_P.ToString();
                B_in = false;
            }
        }

        private void Gal_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (L_in == true)
            {
                double L_G = val_ent / 3.785412;
                tb_out.Text = L_G.ToString();
                L_in = false;
            }
            if (M_in == true)
            {
                double M_G = val_ent / 3785.412;
                tb_out.Text = M_G.ToString();
                G_in = false;
            }
            if (O_in == true)
            {
                double O_G = val_ent / 128;
                tb_out.Text = O_G.ToString();
                O_in = false;
            }
            if (P_in == true)
            {
                double P_G = val_ent /8;
                tb_out.Text = P_G.ToString();
                P_in = false;
            }
            if (G_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                G_in = false;
            }
            if (B_in == true)
            {
                double B_G = val_ent / 0.0238095;
                tb_out.Text = B_G.ToString();
                B_in = false;
            }
        }

        private void Barril_out_Click(object sender, EventArgs e)
        {
            val_ent = Convert.ToDouble(tb_in.Text);
            if (L_in == true)
            {
                double L_B = val_ent / 158.987;
                tb_out.Text = L_B.ToString();
                L_in = false;
            }
            if (M_in == true)
            {
                double M_B = val_ent / 158987.294;
                tb_out.Text = M_B.ToString();
                M_in = false;
            }
            if (O_in == true)
            {
                double O_B = val_ent / 5376;
                tb_out.Text = O_B.ToString();
                O_in = false;
            }
            if (P_in == true)
            {
                double P_B = val_ent / 336;
                tb_out.Text = P_B.ToString();
                P_in = false;
            }
            if (G_in == true)
            {
                double G_B = val_ent / 42;
                tb_out.Text = G_B.ToString();
                G_in = false;
            }
            if (B_in == true)
            {
                MessageBox.Show("El tipo de cambio es el mismo\nIntenta con otro");
                B_in = false;
            }
        }

        private void Volumen_Load(object sender, EventArgs e)
        {

        }

        
    }
}
