﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversor_de_unidades
{
    public partial class Masa : Form
    {
        public Masa()
        {
            InitializeComponent();
        }
        double tan, resultado;
        private void Masa_Load(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnTon_Click(object sender, EventArgs e)
        {
            lblunidad.Text = "Ton";
        }

        private void btnlibra_Click(object sender, EventArgs e)
        {
            lblunidad.Text = "lb";
        }

        private void btnonza_Click(object sender, EventArgs e)
        {
            lblunidad.Text = "oz";
        }

        private void btnkilo_Click(object sender, EventArgs e)
        {
            lblunidad.Text = "kg";
        }

        private void btngramo_Click(object sender, EventArgs e)
        {
            lblunidad.Text = "g";
        }

        private void btnquilate_Click(object sender, EventArgs e)
        {
            lblunidad.Text = "c t";
        }
        //Botones de Operacion

        private void btnT_Click(object sender, EventArgs e)
        {
            tan = Convert.ToDouble(tbValor.Text);
            switch (lblunidad.Text)
            {
                case "Ton":
                    resultado = tan * 1;
                    lblconversion.Text = Convert.ToString(resultado) + " Toneladas(ton)";
                    break;
                case "lb":
                    resultado = tan * 0.0004535;
                    lblconversion.Text = Convert.ToString(resultado) + " Toneladas(ton)";
                    break;
                case "oz":
                    resultado = tan * 0.00002834;
                    lblconversion.Text = Convert.ToString(resultado) + " Toneladas(ton)";
                    break;
                case "kg":
                    resultado = tan * 0.001;
                    lblconversion.Text = Convert.ToString(resultado) + " Toneladas(ton)";
                    break;
                case "g":
                    resultado = tan * 0.000001;
                    lblconversion.Text = Convert.ToString(resultado) + " Toneladas(ton)";
                    break;
                case "c t":
                    resultado = tan * 0.0000002;
                    lblconversion.Text = Convert.ToString(resultado) + " Toneladas(ton)";
                    break;
                default:
                    MessageBox.Show("Por favor ingrese la Unidad");
                    break;
            }
        }

        private void btnOz_Click(object sender, EventArgs e)
        {
            tan = Convert.ToDouble(tbValor.Text);
            switch (lblunidad.Text)
            {
                case "Ton":
                    resultado = tan * 35273.9619;
                    lblconversion.Text = Convert.ToString(resultado) + " Onzas(oz)";
                    break;
                case "lb":
                    resultado = tan * 16;
                    lblconversion.Text = Convert.ToString(resultado) + " Onzas(oz)";
                    break;
                case "oz":
                    resultado = tan * 1;
                    lblconversion.Text = Convert.ToString(resultado) + " Onzas(oz)";
                    break;
                case "kg":
                    resultado = tan * 35.2739;
                    lblconversion.Text = Convert.ToString(resultado) + " Onzas(oz)";
                    break;
                case "g":
                    resultado = tan * 0.03527;
                    lblconversion.Text = Convert.ToString(resultado) + " Onzas(oz)";
                    break;
                case "c t":
                    resultado = tan * 0.007054;
                    lblconversion.Text = Convert.ToString(resultado) + " Onzas(oz)";
                    break;
                default:
                    MessageBox.Show("Por favor ingrese la Unidad");
                    break;
            }
        }

        private void btnKg_Click(object sender, EventArgs e)
        {
            tan = Convert.ToDouble(tbValor.Text);
            switch (lblunidad.Text)
            {
                case "Ton":
                    resultado = tan * 1000;
                    lblconversion.Text = Convert.ToString(resultado) + " Kilogramos(kg)";
                    break;
                case "lb":
                    resultado = tan * 0.4535;
                    lblconversion.Text = Convert.ToString(resultado) + " Kilogramos(kg)";
                    break;
                case "oz":
                    resultado = tan * 0.02834;
                    lblconversion.Text = Convert.ToString(resultado) + " Kilogramos(kg)";
                    break;
                case "kg":
                    resultado = tan * 1;
                    lblconversion.Text = Convert.ToString(resultado) + " Kilogramos(kg)";
                    break;
                case "g":
                    resultado = tan * 0.001;
                    lblconversion.Text = Convert.ToString(resultado) + " Kilogramos(kg)";
                    break;
                case "c t":
                    resultado = tan * 0.0002;
                    lblconversion.Text = Convert.ToString(resultado) + " Kilogramos(kg)";
                    break;
                default:
                    MessageBox.Show("Por favor ingrese la Unidad");
                    break;
            }
        }

        private void btnLb_Click(object sender, EventArgs e)
        {
            tan = Convert.ToDouble(tbValor.Text);
            switch (lblunidad.Text)
            {
                case "Ton":
                    resultado = tan * 2204.622;
                    lblconversion.Text = Convert.ToString(resultado) + " Libras(lb)";
                    break;
                case "lb":
                    resultado = tan * 1;
                    lblconversion.Text = Convert.ToString(resultado) + " Libras(lb)";
                    break;
                case "oz":
                    resultado = tan * 0.0625;
                    lblconversion.Text = Convert.ToString(resultado) + " Libras(lb)";
                    break;
                case "kg":
                    resultado = tan * 2.2046;
                    lblconversion.Text = Convert.ToString(resultado) + " Libras(lb)";
                    break;
                case "g":
                    resultado = tan * 0.00220;
                    lblconversion.Text = Convert.ToString(resultado) + " Libras(lb)";
                    break;
                case "c t":
                    resultado = tan * 0.000440;
                    lblconversion.Text = Convert.ToString(resultado) + " Libras(lb)";
                    break;
                default:
                    MessageBox.Show("Por favor ingrese la Unidad");
                    break;
            }
        }

        private void btnG_Click(object sender, EventArgs e)
        {
            tan = Convert.ToDouble(tbValor.Text);
            switch (lblunidad.Text)
            {
                case "Ton":
                    resultado = tan * 1000000;
                    lblconversion.Text = Convert.ToString(resultado) + " gramos(g)";
                    break;
                case "lb":
                    resultado = tan * 453.592;
                    lblconversion.Text = Convert.ToString(resultado) + " gramos(g)";
                    break;
                case "oz":
                    resultado = tan * 28.349;
                    lblconversion.Text = Convert.ToString(resultado) + " gramos(g)";
                    break;
                case "kg":
                    resultado = tan * 1000;
                    lblconversion.Text = Convert.ToString(resultado) + " gramos(g)";
                    break;
                case "g":
                    resultado = tan * 1;
                    lblconversion.Text = Convert.ToString(resultado) + " gramos(g)";
                    break;
                case "c t":
                    resultado = tan * 0.2;
                    lblconversion.Text = Convert.ToString(resultado) + " gramos(g)";
                    break;
                default:
                    MessageBox.Show("Por favor ingrese la Unidad");
                    break;
            }
        }

        

        private void btnct_Click(object sender, EventArgs e)
        {
            tan = Convert.ToDouble(tbValor.Text);
            switch (lblunidad.Text)
            {
                case "Ton":
                    resultado = tan * 5000000;
                    lblconversion.Text = Convert.ToString(resultado) + " Quilates(ct)";
                    break;
                case "lb":
                    resultado = tan * 2267.96;
                    lblconversion.Text = Convert.ToString(resultado) + " Quilates(ct)";
                    break;
                case "oz":
                    resultado = tan * 141.748;
                    lblconversion.Text = Convert.ToString(resultado) + " Quilates(ct)";
                    break;
                case "kg":
                    resultado = tan * 5000;
                    lblconversion.Text = Convert.ToString(resultado) + " Quilates(ct)";
                    break;
                case "g":
                    resultado = tan * 5;
                    lblconversion.Text = Convert.ToString(resultado) + " Quilates(ct)";
                    break;
                case "c t":
                    resultado = tan * 1;
                    lblconversion.Text = Convert.ToString(resultado) + " Quilates(ct)";
                    break;
                default:
                    MessageBox.Show("Por favor ingrese la Unidad");
                    break;
            }
        }

    }
}
