﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Conversor_de_unidades
{
    public partial class inicio : Form
    {
        public inicio()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Moneda mon = new Moneda();
            mon.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Masa ma = new Masa();
            ma.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Volumen vol = new Volumen();
            vol.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Longitúd lon = new Longitúd();
            lon.Show();
        }
    }
}
