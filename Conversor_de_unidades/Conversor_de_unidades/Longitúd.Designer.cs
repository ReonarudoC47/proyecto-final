﻿namespace Conversor_de_unidades
{
    partial class Longitúd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_out = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_in = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AL_in = new System.Windows.Forms.Button();
            this.Pie_in = new System.Windows.Forms.Button();
            this.Metro_in = new System.Windows.Forms.Button();
            this.Par_in = new System.Windows.Forms.Button();
            this.Milla_in = new System.Windows.Forms.Button();
            this.Kilo_in = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.AL_out = new System.Windows.Forms.Button();
            this.Pie_out = new System.Windows.Forms.Button();
            this.Metro_out = new System.Windows.Forms.Button();
            this.Par_out = new System.Windows.Forms.Button();
            this.Kilo_out = new System.Windows.Forms.Button();
            this.Milla_out = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_out
            // 
            this.tb_out.Location = new System.Drawing.Point(202, 195);
            this.tb_out.Name = "tb_out";
            this.tb_out.ReadOnly = true;
            this.tb_out.Size = new System.Drawing.Size(146, 20);
            this.tb_out.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(199, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Valor a cambiado:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Nuevo volumen";
            // 
            // tb_in
            // 
            this.tb_in.Location = new System.Drawing.Point(202, 69);
            this.tb_in.Name = "tb_in";
            this.tb_in.Size = new System.Drawing.Size(146, 20);
            this.tb_in.TabIndex = 50;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(183, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 13);
            this.label2.TabIndex = 49;
            this.label2.Text = "Valor a cambiar (usar \",\" para decimales):";
            // 
            // AL_in
            // 
            this.AL_in.Location = new System.Drawing.Point(93, 95);
            this.AL_in.Name = "AL_in";
            this.AL_in.Size = new System.Drawing.Size(75, 23);
            this.AL_in.TabIndex = 48;
            this.AL_in.Text = "Año luz";
            this.AL_in.UseVisualStyleBackColor = true;
            // 
            // Pie_in
            // 
            this.Pie_in.Location = new System.Drawing.Point(93, 66);
            this.Pie_in.Name = "Pie_in";
            this.Pie_in.Size = new System.Drawing.Size(75, 23);
            this.Pie_in.TabIndex = 47;
            this.Pie_in.Text = "Pie";
            this.Pie_in.UseVisualStyleBackColor = true;
            // 
            // Metro_in
            // 
            this.Metro_in.Location = new System.Drawing.Point(93, 37);
            this.Metro_in.Name = "Metro_in";
            this.Metro_in.Size = new System.Drawing.Size(75, 23);
            this.Metro_in.TabIndex = 46;
            this.Metro_in.Text = "Metro";
            this.Metro_in.UseVisualStyleBackColor = true;
            // 
            // Par_in
            // 
            this.Par_in.Location = new System.Drawing.Point(12, 95);
            this.Par_in.Name = "Par_in";
            this.Par_in.Size = new System.Drawing.Size(75, 23);
            this.Par_in.TabIndex = 45;
            this.Par_in.Text = "Parsec";
            this.Par_in.UseVisualStyleBackColor = true;
            // 
            // Milla_in
            // 
            this.Milla_in.Location = new System.Drawing.Point(12, 66);
            this.Milla_in.Name = "Milla_in";
            this.Milla_in.Size = new System.Drawing.Size(75, 23);
            this.Milla_in.TabIndex = 44;
            this.Milla_in.Text = "Milla";
            this.Milla_in.UseVisualStyleBackColor = true;
            // 
            // Kilo_in
            // 
            this.Kilo_in.Location = new System.Drawing.Point(12, 37);
            this.Kilo_in.Name = "Kilo_in";
            this.Kilo_in.Size = new System.Drawing.Size(75, 23);
            this.Kilo_in.TabIndex = 43;
            this.Kilo_in.Text = "Kilometro";
            this.Kilo_in.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Longitud a cambiar";
            // 
            // AL_out
            // 
            this.AL_out.Location = new System.Drawing.Point(93, 221);
            this.AL_out.Name = "AL_out";
            this.AL_out.Size = new System.Drawing.Size(75, 23);
            this.AL_out.TabIndex = 59;
            this.AL_out.Text = "Año luz";
            this.AL_out.UseVisualStyleBackColor = true;
            // 
            // Pie_out
            // 
            this.Pie_out.Location = new System.Drawing.Point(93, 192);
            this.Pie_out.Name = "Pie_out";
            this.Pie_out.Size = new System.Drawing.Size(75, 23);
            this.Pie_out.TabIndex = 58;
            this.Pie_out.Text = "Pie";
            this.Pie_out.UseVisualStyleBackColor = true;
            // 
            // Metro_out
            // 
            this.Metro_out.Location = new System.Drawing.Point(93, 163);
            this.Metro_out.Name = "Metro_out";
            this.Metro_out.Size = new System.Drawing.Size(75, 23);
            this.Metro_out.TabIndex = 57;
            this.Metro_out.Text = "Metro";
            this.Metro_out.UseVisualStyleBackColor = true;
            // 
            // Par_out
            // 
            this.Par_out.Location = new System.Drawing.Point(12, 221);
            this.Par_out.Name = "Par_out";
            this.Par_out.Size = new System.Drawing.Size(75, 23);
            this.Par_out.TabIndex = 56;
            this.Par_out.Text = "Parsec";
            this.Par_out.UseVisualStyleBackColor = true;
            // 
            // Kilo_out
            // 
            this.Kilo_out.Location = new System.Drawing.Point(12, 163);
            this.Kilo_out.Name = "Kilo_out";
            this.Kilo_out.Size = new System.Drawing.Size(75, 23);
            this.Kilo_out.TabIndex = 54;
            this.Kilo_out.Text = "Kilometro";
            this.Kilo_out.UseVisualStyleBackColor = true;
            // 
            // Milla_out
            // 
            this.Milla_out.Location = new System.Drawing.Point(12, 192);
            this.Milla_out.Name = "Milla_out";
            this.Milla_out.Size = new System.Drawing.Size(75, 23);
            this.Milla_out.TabIndex = 60;
            this.Milla_out.Text = "Milla";
            this.Milla_out.UseVisualStyleBackColor = true;
            // 
            // Longitúd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 261);
            this.Controls.Add(this.Milla_out);
            this.Controls.Add(this.AL_out);
            this.Controls.Add(this.Pie_out);
            this.Controls.Add(this.Metro_out);
            this.Controls.Add(this.Par_out);
            this.Controls.Add(this.Kilo_out);
            this.Controls.Add(this.tb_out);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_in);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AL_in);
            this.Controls.Add(this.Pie_in);
            this.Controls.Add(this.Metro_in);
            this.Controls.Add(this.Par_in);
            this.Controls.Add(this.Milla_in);
            this.Controls.Add(this.Kilo_in);
            this.Controls.Add(this.label1);
            this.Name = "Longitúd";
            this.Text = "Longitúd";
            this.Load += new System.EventHandler(this.Longitúd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tb_out;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_in;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AL_in;
        private System.Windows.Forms.Button Pie_in;
        private System.Windows.Forms.Button Metro_in;
        private System.Windows.Forms.Button Par_in;
        private System.Windows.Forms.Button Milla_in;
        private System.Windows.Forms.Button Kilo_in;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AL_out;
        private System.Windows.Forms.Button Pie_out;
        private System.Windows.Forms.Button Metro_out;
        private System.Windows.Forms.Button Par_out;
        private System.Windows.Forms.Button Kilo_out;
        private System.Windows.Forms.Button Milla_out;
    }
}